# Test Project for Agile Engine

**Version 1.0.0**

Application that analyzes HTML and finds a specific element

User should provide
- URL (e.g. "https://agileengine.bitbucket.io/keFivpUlPMtzhfAy/samples/sample-0-origin.html")
- Text of Element to Search (e.g. "Make everything OK")

To Exit please type "Exit" (case insensitive)

Application recursively traverses the tree of HTML Nodes 
and collect to Map Text and Path of the first matched Node.
If remove the flag of found Element, the application will collect all matched Nodes

As a result, the user will get Text and Path
Text : Make everything OK
Path : [html[1], body[2], div[1], div[5], div[9], div[1], div[1], div[9], a[1]]

---
## Test Data
- 1
    - https://agileengine.bitbucket.io/keFivpUlPMtzhfAy/samples/sample-0-origin.html
    - Make everything OK
    - [html[1], body[2], div[1], div[5], div[9], div[1], div[1], div[5], a[1]]
- 2
    - https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-1-evil-gemini.html
    - Make everything OK
    - [html[1], body[2], div[1], div[5], div[9], div[1], div[1], div[5], a[3]]
- 3
    - https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-3-the-escape.html
    - Do anything perfect
    - [html[1], body[2], div[1], div[5], div[9], div[1], div[1], div[9], a[1]]
- 4
    - https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-4-the-mash.html
    - Do all GREAT
    - [html[1], body[2], div[1], div[5], div[9], div[1], div[1], div[9], a[1]]

---
## Contributors

- Dmytro Latsiuha <ldg283@gmail.com>