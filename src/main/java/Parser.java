import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    private static Map<String, Queue<String>> pathToElements = new HashMap<>();
    private static boolean elementWithTextFound = false;

    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputURL;
        boolean isURLCorrect;

        do {
            System.out.println("Please type valid URL or Exit");
            inputURL = bufferedReader.readLine();

            if ("EXIT".equals(inputURL.toUpperCase())) {
                return;
            }

            Pattern p = Pattern.compile("(https://)?(http://)?[a-zA-Z_0-9\\-]+(\\.\\w[a-zA-Z_0-9\\-]+)+(/[#&\\n\\-=?+%/.\\w]+)?");
            Matcher m = p.matcher(inputURL);
            isURLCorrect = m.matches();
        } while (!isURLCorrect);

        String textToSearch;
        System.out.println("Please type Text to search");
        textToSearch = bufferedReader.readLine();

        Document document = Jsoup.connect(inputURL).get();
        searchElementByText(document.childNodes(), textToSearch);

        if (!pathToElements.isEmpty()) {
            System.out.println("***********************************");
            pathToElements.forEach((k, v) -> {
                System.out.println("Text : " + k);
                System.out.println("Path : " + v);
            });
        }
    }

    private static void searchElementByText(List<Node> nodes, String textToSearch) {
        for (Node node : nodes) {
            if (elementWithTextFound) {
                return;
            }

            List<Node> childNodes = node.childNodes();

            if (childNodes.size() == 1) {
                Node firstChildNode = node.childNode(0);
                if (firstChildNode instanceof TextNode) {
                    String text = ((TextNode) firstChildNode).text();
                    if (textToSearch.equals(text.trim())) {
                        System.out.println(text);
                        elementWithTextFound = true;
                        pathToElements.put(textToSearch, getPathToElement(node));
                    }
                }
            }

            if (!childNodes.isEmpty()) {
                searchElementByText(childNodes, textToSearch);
            }
        }
    }

    private static Queue<String> getPathToElement(Node node) {
        Deque<String> path = new ArrayDeque<>();
        do {
            path.push(String.format("%s[%s]", node.nodeName(), node.siblingIndex()));
            node = node.parent();
        } while (!"#document".equals(node.nodeName()));
        return path;
    }
}